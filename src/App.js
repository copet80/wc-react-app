import React from 'react';
import cx from 'classnames';

class App extends React.Component {
  state = {
    on: false,
  };

  constructor(props) {
    super(props);
    this.lightBulbRef = React.createRef();
    this.switchButtonRef = React.createRef();
  }

  componentDidMount() {
    if (this.lightBulbRef.current) {
      this.lightBulbRef.current.addEventListener('change', this.handleChange);
    }
    if (this.switchButtonRef.current) {
      this.switchButtonRef.current.addEventListener('change', this.handleChange);
    }
  }

  handleChange = event => {
    const { on } = event.detail;
    this.setState({ on });
  };

  render() {
    const { on } = this.state;

    return (
      <div className={cx('App', { on: on === 'true' })}>
        <light-bulb ref={this.lightBulbRef} class="myLightBulb" on={on}></light-bulb>
        <switch-button ref={this.switchButtonRef} class="mySwitchButton" on={on}></switch-button>
      </div>
    );
  }
}

export default App;
