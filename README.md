> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


# Get Started

### 1. Install all dependencies

`yarn`

### 2. Run the app

`yarn start`

### 3. Build the components

`yarn build`

